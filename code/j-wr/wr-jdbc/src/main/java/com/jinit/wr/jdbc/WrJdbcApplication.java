package com.jinit.wr.jdbc;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * WrJdbcApplication
 *
 * @author JInit
 * @date 2021-11-10
 */
@SpringBootApplication
public class WrJdbcApplication {
}
