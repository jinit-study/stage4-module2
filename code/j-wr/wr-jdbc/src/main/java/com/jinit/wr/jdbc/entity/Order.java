package com.jinit.wr.jdbc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Order 订单
 *
 * @author JInit
 * @date 2021-11-10
 */
@Entity
@Table(name = "c_order", indexes = {@Index(name = "idx_userId_positionId", columnList = "user_id,position_id"),
        @Index(name = "idx_userId_operateTime", columnList = "user_id,update_time")})
public class Order implements Serializable {
    /**
     * 主键ID
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * 是否被删除
     */
    @Column(name = "is_del")
    private Boolean isDel;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 公司id
     */
    @Column(name = "company_id")
    private Integer companyId;

    /**
     * B端用户id
     */
    @Column(name = "publish_user_id")
    private Integer publishUserId;

    /**
     * 职位ID
     */
    @Column(name = "position_id")
    private long positionId;

    /**
     * 简历类型:0附件 1在线
     */
    @Column(name = "resume_type")
    private Integer resumeType;

    /**
     * 投递状态
     * WAIT-待处理理
     * AUTO_FILTER-⾃自动过滤
     * PREPARE_CONTACT-待沟通
     * REFUSE-拒绝
     * ARRANGE_INTERVIEW-通知⾯面试
     */
    @Column(name = "status")
    private String status;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 处理时间
     */
    @Column(name = "`update_time`")
    private Date updateTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getDel() {
        return isDel;
    }

    public void setDel(Boolean del) {
        isDel = del;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getPublishUserId() {
        return publishUserId;
    }

    public void setPublishUserId(Integer publishUserId) {
        this.publishUserId = publishUserId;
    }

    public long getPositionId() {
        return positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }

    public Integer getResumeType() {
        return resumeType;
    }

    public void setResumeType(Integer resumeType) {
        this.resumeType = resumeType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", isDel=" + isDel +
                ", userId=" + userId +
                ", companyId=" + companyId +
                ", publishUserId=" + publishUserId +
                ", positionId=" + positionId +
                ", resumeType=" + resumeType +
                ", status='" + status + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
