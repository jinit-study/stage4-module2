package com.jinit.wr.jdbc.dao;

import com.jinit.wr.jdbc.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * OrderRepository
 *
 * @author JInit
 * @date 2021-11-10
 */
public interface OrderRepository extends JpaRepository<Order, Long> {
}
