package com.jinit.wr.jdbc;

import com.jinit.wr.jdbc.entity.Order;
import com.jinit.wr.jdbc.dao.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * ShardingWrTest
 * c_order数据分片 读写分离测试
 *
 * @author JInit
 * @date 2021-11-10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ShardingWrTest {

    @Resource
    private OrderRepository orderRepository;

    /**
     * 测试数据分片
     */
    @Test
    public void testOrderWrite() {
        Random random = new Random();
        for (int i = 1; i <= 4; i++) {
            // 模拟用户ID、公司ID
            int companyId = random.nextInt(10);
            int userId = i;
            int positionId = random.nextInt(10);
            int publishUserId = random.nextInt(10);
            // 保存订单信息
            Order order = new Order();
            order.setDel(false);
            order.setCompanyId(companyId);
            order.setPositionId(positionId);
            order.setUserId(userId);
            order.setPublishUserId(publishUserId);
            order.setResumeType(1);
            order.setStatus("AUTO");
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            orderRepository.save(order);
        }
    }

    /**
     * 测试读从库
     */
    @Test
    public void testOrderRead() {
        for (int i = 1; i <= 3; i++) {
            List<Order> list = orderRepository.findAll();
            for (Order order : list) {
                System.out.println("第"+i+"次查询："+order);
            }
        }
    }

    /**
     * 清空数据
     */
    @Test
    public void deleteOrder() {
        orderRepository.deleteAll();
    }

}
